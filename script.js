/**
 * Created by Akyno on 21/06/2016.
 */
function selectAdj(position,id,color){
    var colorx=null;
    switch (position){
        case "up":
            colorx =parseInt(id)-10;
            colorx=("00"+colorx).slice(-2);
            break;
        case "down":
            colorx =parseInt(id)+10;
            colorx=("00"+colorx).slice(-2);
            break;
        case "left":
            colorx =parseInt(id)-1;
            colorx=("00"+colorx).slice(-2);
            break;
        case "right":
            colorx =parseInt(id)+1;
            colorx=("00"+colorx).slice(-2);
            break;
        default:
            throw new Error('Erreur de position dans la fonction selectAdj')
    }
    recolor(colorx,color);
}
function recolor(id,color) {
    var colorA = document.getElementById(id);
    colorA.style.background = color;
}
function color(selected,color){
    identifier = selected.id;
    selected.style.background = color;

    //Dans le cas où on est à gauche du tableau
    if (identifier%10 !==0)
        selectAdj("left",identifier,color);

    //Dans le cas où on est à droite du tableau
    if (identifier%10 !==9) {
        selectAdj("right",identifier,color);
    }

    //Dans le cas où on est en haut du tableau
    if (identifier>=10) {
        selectAdj("up",identifier,color);
    }

    //Dans le cas où on est en bas du tableau
    if (identifier<90) {
        selectAdj("down",identifier,color);
    }
    return identifier;
}